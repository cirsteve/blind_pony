import React, { Component } from 'react'
import { drizzleConnect } from 'drizzle-react'
import PropTypes from 'prop-types'

import getIpfsContent from '../../util/getIpfsContent'

class Content extends Component {

  componentDidMount = () => this.props.getIpfs(this.props.hash, true)

  render () {
    const { hashedContent, hash } = this.props
    let message = `loading message ${hash}`;

    if (hashedContent[hash]) {
      message = hashedContent[hash]
    }

    return (
      <div>
        {message}
      </div>
    )
  }
}

Content.contextTypes = {
  drizzle: PropTypes.object
}

const mapState = state => {
  return {
    hashedContent: state.data.hashedContent
  }
}

const mapDispatch = dispatch => ({
  getIpfs: (hash, noParse) => getIpfsContent(dispatch, hash, noParse)
})

export default drizzleConnect(Content, mapState, mapDispatch);
