import Messages from '../build/contracts/Messages.json'

const drizzleOptions = {
  web3: {
    block: false,
    fallback: {
      type: 'ws',
      url: 'ws://127.0.0.1:8545'
    }
  },
  contracts: [
    Messages
  ],
  events: {
    Messages: [{eventName:'MessageAdded', eventOptions: {fromBlock: 0}}]
  },
  polls: {
    accounts: 1500
  }
}

export default drizzleOptions
