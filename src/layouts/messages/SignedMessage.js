import React, { Component } from 'react'
import { drizzleConnect } from 'drizzle-react'
import PropTypes from 'prop-types'
import Typography from '@material-ui/core/Typography';
import IpfsString from '../common/IpfsString'
import SigVerification from './SigVerification'
import Delete from '../ui/DeleteMessage';
class Content extends Component {
  render () {
    const { message, blockNumber, index } = this.props
    return (
      <div>
        <Typography variant="h6" component="h6"color="inherit">
          <IpfsString hash={message} />
        </Typography>

        <SigVerification {...this.props} blockNumber={blockNumber} />
        <Delete index={index} />

      </div>
    )
  }
}

Content.contextTypes = {
  drizzle: PropTypes.object
}

const mapState = state => {
  return {
    web3: state.web3Instance.web3Instance
  }
}

export default drizzleConnect(Content, mapState);
