import React, { Component } from 'react'
import { drizzleConnect } from 'drizzle-react'
import PropTypes from 'prop-types'
import { getMultihash } from '../../util/multihash'
import MessageContent from './MessageContent'
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';

const styles = theme => ({
  root: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
    marginBottom:  theme.spacing.unit * 4,
  },
});
class Item extends Component {
  constructor (props, context) {
    super(props)
    this.messageKey = context.drizzle.contracts
      .Messages.methods.getMessage.cacheCall(props.index)
    this.state = {
      content: null
    }
  }

  getRenderValues = () => {
    return {
      messageResponse: this.props.Messages.getMessage[this.messageKey] ?
        Object.values(this.props.Messages.getMessage[this.messageKey].value) : null,
    }
  }

  render () {
    const { classes , events, index} = this.props
    const { messageResponse } = this.getRenderValues();
    let message = 'loading message';


    if (messageResponse) {
      const hash = getMultihash(messageResponse)
      message = <MessageContent index={index} hash={hash} event={events.find(e => e.returnValues.id === messageResponse[3])}/>
    }

    return (
      <div>
      <Paper className={classes.root} elevation={1}>
        {message}
      </Paper>
      </div>

    )
  }
}

Item.contextTypes = {
  drizzle: PropTypes.object
}

const mapState = state => {
  return {
    Messages: state.contracts.Messages,
  }
}

export default drizzleConnect(withStyles(styles)(Item), mapState);
