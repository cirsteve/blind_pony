import React, { Component } from 'react'
import { range } from 'lodash'
import { withStyles } from '@material-ui/core/styles';
import MessageItem from './MessageItem'
import DeleteAll from '../ui/DeleteMessages'

class List extends Component {

  render () {
    const { events, messageCount, classes } = this.props

    let messages = 'Loading Messages';
    if (messageCount !== null) {
      if (messageCount) {
        messages = range(messageCount).map((_,idx) => <MessageItem key={idx} index={idx} events={events}/>)
      } else {
        messages = 'No messages for user'
      }
    }

    return (
      <div className={classes.root}>
        <div className={classes.center}>
          {messageCount > 1 ? <DeleteAll /> : null}
        </div>
        {messages}
      </div>
    )
  }
}

const styles = {
  root: {
    marginTop: '1em'
  },
  center: {
    display: 'flex',
    justifyContent: 'center'
  }
};

export default withStyles(styles)(List)
