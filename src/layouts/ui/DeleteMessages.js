import React, { Component } from 'react'
import { drizzleConnect } from 'drizzle-react'
import PropTypes from 'prop-types'
import Button from '@material-ui/core/Button';

class Delete extends Component {

  delete = () => {
    this.context.drizzle.contracts.Messages.methods.deleteMessages.cacheSend();
  }

  render () {
    return (
      <Button variant="contained" color="secondary" onClick={this.delete}>
        Delete All Messages
      </Button>
    )
  }
}

Delete.contextTypes = {
  drizzle: PropTypes.object
}

export default drizzleConnect(Delete);
