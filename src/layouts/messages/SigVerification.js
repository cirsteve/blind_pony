import React, { Component } from 'react'
import { drizzleConnect } from 'drizzle-react'
import PropTypes from 'prop-types'
import Typography from '@material-ui/core/Typography';

class Content extends Component {
  constructor (props, context) {
    super(props)

    this.state = {
      verified: null
    }
  }

  componentDidMount = () => {
    const { address, message, signedMessage, web3 } = this.props
    web3.eth.personal.ecRecover(message, signedMessage, this.verifySigner.bind(this, address))
  }

  verifySigner = (address, err, signer) => {
    console.log('verify sig: ', err, signer, this.props.address)
    this.setState({verified: parseInt(signer, 16) === parseInt(address, 16)})
  }

  render () {
    const { address, blockNumber } = this.props
    let content = `veryifying signature by ${address}`;

    if (this.state.verified === true) {
      content = `signed by ${address}`
    } else if (this.state.verified === false) {
      content = `unable to verify signer ${address}`
    }
    return (
      <div>
      <Typography component="p" color="inherit">
        {content}
      </Typography>
      <Typography component="p" color="inherit">
        Published in block {blockNumber}
      </Typography>
      </div>
    )
  }
}

Content.contextTypes = {
  drizzle: PropTypes.object
}

const mapState = state => {
  return {
    web3: state.web3Instance.web3Instance
  }
}

export default drizzleConnect(Content, mapState);
