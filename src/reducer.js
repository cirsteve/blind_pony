import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'
import { drizzleReducers } from 'drizzle'
import dataReducer from './app/reducers/data'
import web3InstanceReducer from './util/web3/web3Reducer'

const reducer = combineReducers({
  data: dataReducer,
  routing: routerReducer,
  web3Instance: web3InstanceReducer,
  ...drizzleReducers
})

export default reducer
