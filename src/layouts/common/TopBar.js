import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';

const styles = {
  root: {
    flex: 1,
  },
  grow: {
    display: 'flex',
    marginLeft: 'auto',
    marginRight: 'auto'
  },
  logo: {color: 'green'},
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
  item: {
    margin: 10
  },
  text: {
    width: '15em'
  }
};

function ButtonAppBar(props) {
  const { classes } = props;
  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <div className={classes.grow}>

            <h1 className={classes.item}>

              BlindPony

            </h1>

          <div className={`${classes.item} ${classes.text}`}>
            Verified Messaging for the Ethereum Network
          </div>
          </div>

        </Toolbar>
      </AppBar>
    </div>
  );
}

ButtonAppBar.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ButtonAppBar);
