import React, { Component, Fragment } from 'react'
import { drizzleConnect } from 'drizzle-react'
import PropTypes from 'prop-types'
import Button from '@material-ui/core/Button';

import Dialog from '../common/Dialog'

class Create extends Component {
  constructor(props, context) {
    super(props)
    this.state = {
      openInfo: false,
      hashId: null
    }
  }

  updateShowingForm = open => this.setState({...this.state, open})

  updateShowingInfo = openInfo => this.setState({...this.state, openInfo})

  signSubmit = () => {
    const hashId = Math.ceil(Math.random() * 1000000000000);
    let submit = this.context.drizzle.contracts.Messages.methods.addMessage.cacheSend;
    submit = submit.bind(this, this.props.to)
    this.props.signSubmit(this.props.body, submit, hashId, this.props.web3.eth.personal.sign, this.props.address)
    this.setState({...this.state, hashId, openInfo: true})
  }

  render () {
    const {disabled, uploadedHashes } = this.props
    const dialogContent = this.state.hashId && uploadedHashes[this.state.hashId] ?
      'Complete Your Submission wtih MetaMask'
      :
      'Preparing Your Transaction'

    return (
      <Fragment>
        <Button variant='contained' onClick={this.signSubmit} disabled={disabled} color="primary">
          Send
        </Button>
        <Dialog
          open={this.state.openInfo}
          handleClose={this.updateShowingInfo.bind(this, false)}
          content={dialogContent} />
      </Fragment>
    )
  }
}

Create.contextTypes = {
  drizzle: PropTypes.object
}

// May still need this even with data function to refresh component on updates for this contract.
const mapState = state => {
  return {
    Messages: state.contracts.Messages,
    pendingUpload: state.data.pendingUpload,
    uploadedHashes: state.data.uploadedHashes,
    web3: state.web3Instance.web3Instance
  }
}

const mapDispatch = (dispatch) => {
    return {
        submit: (upload, save, hashId) => dispatch({type: 'IPFS_UPLOAD_THEN_SAVE', payload: {upload, save, hashId}}),
        signSubmit: (upload, save, hashId, sign, address) => dispatch({type: 'IPFS_UPLOAD_THEN_SIGN_THEN_SAVE', payload: {upload, save, hashId, sign, address}}),
        ipfsUploadAcked: () => dispatch({type: 'IPFS_UPLOAD_ACKED'})
    };
}

export default drizzleConnect(Create, mapState, mapDispatch);
