pragma solidity ^0.5.0;

import './Multihash.sol';

contract Messages is Multihash {
  uint256 internal messageCount;
  struct Message {
    uint256 id;
    MultiHash data;
  }

  mapping (address => Message[]) internal messages;

  event MessageAdded(address indexed to, uint256 id);

  function addMessage (address _to, bytes32 _hash, uint8 _hashFunction, uint8 _size) public {
    MultiHash memory multihash = MultiHash(_hash, _hashFunction, _size);
    Message memory message = Message(messageCount, multihash);
    messages[_to].push(message);
    messageCount++;
    emit MessageAdded(_to, message.id);
  }

  function deleteMessage (uint256 _index) public {
    require(_index < messages[msg.sender].length, 'Index is not in message list');
    messages[msg.sender][_index] = messages[msg.sender][messages[msg.sender].length-1];
    messages[msg.sender].length--;
  }

  function deleteMessages () public {
    messages[msg.sender].length = 0;
  }

  function getMessageCount () public view returns (uint256) {
    return messages[msg.sender].length;
  }

  function getMessage(uint256 _index) public view returns (bytes32 hash, uint8 hashFunction, uint8 size, uint256 id){
    Message storage message = messages[msg.sender][_index];
    return (message.data.hash, message.data.hashFunction, message.data.size, message.id);
  }

}
