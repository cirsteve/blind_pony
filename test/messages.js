var Messages = artifacts.require("./Messages.sol");

const MULTI_HASH = {
  digest: '0x87f4a3051e34cb48e5d97af1e6c2d311bbc9f0846a087757adcbc392595c98d2',
  function: 18,
  size: 32
}
contract('Messages', function(accounts) {
  var messagesInstance;
  it("...should add a message.", async function() {
    messagesInstance = await Messages.deployed()
    var preAddCount = await messagesInstance.getMessageCount.call();
    assert.equal(preAddCount, 0, "immediately post depoy the message count should be 0");
    console.log('accounts: ', accounts);
    await messagesInstance.addMessage(accounts[0], MULTI_HASH.digest, MULTI_HASH.function, MULTI_HASH.size, {from: accounts[1]});
    var postAddCount = await messagesInstance.getMessageCount.call();
    assert.equal(postAddCount, 1, "the message count was not increased");
  });

  it("...should retrieve a message.", async function() {
    var messageResponse = await messagesInstance.getMessage.call(0);
    assert.equal(messageResponse.hash, MULTI_HASH.digest, "the message digest is incorrect");
    assert.equal(messageResponse.hashFunction, MULTI_HASH.function, "the message hash function is incorrect");
    assert.equal(messageResponse.size, MULTI_HASH.size, "the message size is incorrect");
  });

  it("...should delete a message.", async function() {
    var preDeleteCount = await messagesInstance.getMessageCount.call();
    assert.equal(preDeleteCount, 1, "should be one message");
    await messagesInstance.deleteMessage(0, {from: accounts[0]});
    var postDeleteCount = await messagesInstance.getMessageCount.call();
    assert.equal(postDeleteCount, 0, "the message count did not decrease after delete");
  });

  it("...should delete all messages.", async function() {
    var initialCount = await messagesInstance.getMessageCount.call();
    assert.equal(initialCount, 0, "should be 0 messages intitially");
    await messagesInstance.addMessage(
      accounts[0],
      MULTI_HASH.digest,
      MULTI_HASH.function,
      MULTI_HASH.size,
      {from: accounts[1]}
    );
    await messagesInstance.addMessage(
      accounts[0],
      MULTI_HASH.digest,
      MULTI_HASH.function,
      MULTI_HASH.size,
      {from: accounts[1]}
    );
    var postAddCount = await messagesInstance.getMessageCount.call();
    assert.equal(postAddCount, 2, "the message count did not increase after add");
    await messagesInstance.deleteMessages();
    var postDeleteCount = await messagesInstance.getMessageCount.call();
    assert.equal(postDeleteCount, 0, "the message count did not decrease after delete all");
  });

});
