import React, { Component } from 'react'
import { withStyles } from '@material-ui/core/styles';
import getWeb3 from '../util/web3/getWeb3'
import TopBar from './common/TopBar'
import { drizzleConnect } from 'drizzle-react'
import PropTypes from 'prop-types'
import Messages from './messages/Messages'
import CurrentUser from './common/CurrentUser'
import Typography from '@material-ui/core/Typography';

const styles = {

  body: {
    width: '60%',
    marginTop: '1em',
    marginRight: 'auto',
    marginLeft: 'auto'
  },
  buttons: {
    display: 'flex',
    justifyContent: 'center',
    marginTop: '1em',
    marginBottom: '2em'
  },
  spacer: {
    marginRight: '2em'
  }
};

class User extends Component {
  componentDidMount = () => {
    if (getWeb3) {//hack to prevent unused variable warning
    }
  }

  render () {
    const { address, classes, web3 } = this.props

    const content = parseInt(web3.eth.currentProvider.networkVersion, 10) === 4 || true?
      <Messages address={address} />
      :
      <Typography component="h4" color="inherit">
        Login to Rinkeby to access BlindPony
      </Typography>


    return (
      <div>
        <TopBar address={address} />
        <div className={classes.body}>
          <CurrentUser address={address} />
          {content}
        </div>

      </div>
    )
  }
}

User.contextTypes = {
  drizzle: PropTypes.object
}

// May still need this even with data function to refresh component on updates for this contract.
const mapStateToProps = state => {
  return {
    address: state.accounts[0],
    web3: state.web3Instance.web3Instance
  }
}

export default drizzleConnect(withStyles(styles)(User), mapStateToProps);
