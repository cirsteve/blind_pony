import React, { Component } from 'react'
import { drizzleConnect } from 'drizzle-react'
import PropTypes from 'prop-types'
import Typography from '@material-ui/core/Typography';

import getIpfsContent from '../../util/getIpfsContent'
import SignedMessage from './SignedMessage'
class Content extends Component {

  componentDidMount = () => this.props.getIpfs(this.props.hash, true)

  render () {
    const { hashedContent, hash, event, index } = this.props
    let message = `loading message ${hash}`;
    const blockNumber = event ? event.blockNumber : null

    if (hashedContent[hash]) {
      try {
        message = <SignedMessage {...JSON.parse(hashedContent[hash])} index={index} blockNumber={blockNumber} />
      } catch(e) {
        message =
          <Typography variant="h6" component="h6"color="inherit">
            {hashedContent[hash]}-{blockNumber}
          </Typography>
      }
    }

    return (
      <div>
        {message}
      </div>
    )
  }
}

Content.contextTypes = {
  drizzle: PropTypes.object
}

const mapState = state => {
  return {
    hashedContent: state.data.hashedContent
  }
}

const mapDispatch = dispatch => ({
  getIpfs: (hash, noParse) => getIpfsContent(dispatch, hash, noParse)
})

export default drizzleConnect(Content, mapState, mapDispatch);
