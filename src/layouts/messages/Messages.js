import React, { Component } from 'react'
import { drizzleConnect } from 'drizzle-react'
import PropTypes from 'prop-types'
import Tabs from '../common/Tabs'
import MessageList from './MessageList'
import WriteMessage from './WriteMessage'

class List extends Component {
  constructor (props, context) {
    super(props)
    this.messageCountKey = context.drizzle.contracts.Messages
      .methods.getMessageCount.cacheCall()
  }

  getRenderValues = () => {
    return {
      messageCount: this.props.Messages.getMessageCount[this.messageCountKey] ?
        parseInt(this.props.Messages.getMessageCount[this.messageCountKey].value, 10) : null
    }
  }

  render () {
    const { address, Messages } = this.props
    const events = Messages.events.filter(e => e.returnValues.to === address)
    const { messageCount } = this.getRenderValues();

    const tabs = [
      {
        label: `Inbox (${messageCount})`,
        content: <MessageList messageCount={messageCount} events={events} />
      },
      {
        label: 'Write Message',
        content: <WriteMessage address={address} />
      }
    ]


    return <Tabs tabs={tabs} />
  }
}

List.contextTypes = {
  drizzle: PropTypes.object
}

// May still need this even with data function to refresh component on updates for this contract.
const mapStateToProps = state => {
  return {
    Messages: state.contracts.Messages
  }
}

export default drizzleConnect(List, mapStateToProps);
