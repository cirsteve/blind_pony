import React, { Component, Fragment } from 'react'

import Text from '../common/Text'
import CreateMessage from '../ui/CreateMessage'
const fields = [
  {
    name: 'to',
    label: 'To',
    type: 'text',
    placeholder: '0x...',
    default: ''
  },
  {
    name: 'body',
    label: 'Message',
    type: 'text',
    multiline: true,
    default: ''
  }
]

class Write extends Component {
  constructor (props) {
    super(props)
    this.state = {
      fields: fields.reduce((acc, f) => {
        acc[f.name] = f.default;
        return acc;
      }, {}),
    }
  }

  updateField = (field, e, val) =>
    this.setState({
      ...this.state,
      fields: {...this.state.fields, [field]: e.target.value}
      }
    )

  render () {

    const renderFields = fields.map(f => {
      f.value = this.state.fields[f.name]
      f.onChange = this.updateField.bind(this, f.name)
      f.key = f.name
      return f;
    }).map(f => <Text {...f} />)

    return (
      <Fragment>
        <div style={{marginBottom: '1em'}}>
          {renderFields}
        </div>
        <CreateMessage to={this.state.fields.to} body={this.state.fields.body} address={this.props.address} disabled={!this.state.fields.body || !this.state.fields.to}/>
      </Fragment>
    )
  }
}



export default Write
