import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import Typography from '@material-ui/core/Typography';
import Blockie from './Blockie';


const styles = {
  root: {
    flexGrow: 1,
    textAlign: 'center'
  },
  grow: {
    flexGrow: 1,
  },
  logo: {color: 'green'},
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
};

function CurrentUser(props) {
  const { classes, address } = props;
  return (
    <div className={classes.root}>
          <Blockie address={address} />

          <Typography variant="h5" color="inherit" className={classes.grow}>

              {address}

          </Typography>

    </div>
  );
}

CurrentUser.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(CurrentUser);
